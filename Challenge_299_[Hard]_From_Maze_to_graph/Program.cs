﻿using Challenge_298_Hard_From_Maze_to_graph.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Challenge_298_Hard_From_Maze_to_graph
{
    class Program
    {
        //A* Pathfinder for mazes. Goal is to find shortest path between all numbers starting from 0.
        //Step 1. 
        //Read the maze, find all paths, walls and numbers.
        //Step 2.
        //Pathfind between all possible paths to find the cost and path.
        //Step 3.
        //Find all permutations of the numbers then use them to find the lowest cost path that include all numbers.
        static void Main(string[] args)
        {
            //Print the maze, find all walls, numbers, and pathways.
            Console.SetWindowSize(179, 37);
            Console.CursorVisible = false;
            TextfileReader reader = new TextfileReader();
            ConsoleWriter writer = new ConsoleWriter(reader);
            writer.PrintMaze("Maze.txt");
            Pathfinder pathfinder = new Pathfinder(reader, writer);
            List<Coordinate> sortedNumbers = reader.FindAllNumbers("Maze.txt");
            List<Path> allPaths = new List<Path>();
 
            //Foreach coordinate in sortedNumbers...
            int x = 0;
            foreach (Coordinate c in sortedNumbers)
            {
                //Loop all coorinates in sortedNumbers...
                for (int i = x; i < sortedNumbers.Count; i++)
                {
                    //If a path between two coordinates does not exist and both coordinates is not the same...
                    if (c.number != i && !allPaths.Contains(new Path(c, sortedNumbers[i])))
                    {
                        //Find the best path between those two coordinates and add it to allPaths.
                        allPaths.Add(pathfinder.FindPath(c, sortedNumbers[i]));
                    }
                }
                x++;
            }

            //Get all avaiable permutations for our numbers.
            List<int> numbers = new List<int>();
            numbers.Add(1);
            numbers.Add(2);
            numbers.Add(3);
            numbers.Add(4);
            numbers.Add(5);
            numbers.Add(6);
            numbers.Add(7);
            List<int[]> perms = Permutation.GetPerm(numbers.ToArray()).ToList();

            int minCost = 0;

            //Foreach permutation...
            foreach (int[] array in perms)
            {
                int cost = 0;
                bool running = true;
                string perm = "";

                //Check each number in the permutation...
                for (int i = 0; i < array.Length; i++)
                {
                    //Aslong it not the last one in the permutation...
                    if (i != array.Length - 1)
                    {
                        //Check for a match between current number and next.
                        foreach (Path p in allPaths)
                        {
                            //Always check first between 0 and first number in permutation.
                            if(cost == 0 && p.Equals(new Path(sortedNumbers[0], sortedNumbers[array[i]])))
                            {
                                perm += "0";
                                cost += p.totalCost;
                            }

                            //Otherwise check between current number and next.
                            if(cost > 0 && p.Equals(new Path(sortedNumbers[array[i]], sortedNumbers[array[i + 1]])))
                            {
                                perm += array[i];
                                cost += p.totalCost;
                                //Check if cost goes above the mininmum cost, if so goto next permutation. 
                                //(Discard additional results if cost is same)
                                if (minCost <= cost && minCost != 0)
                                {
                                    running = false;
                                    break;
                                } 
                            }
                        }   
                    }
                    else
                    {
                        perm += array[i];
                    }

                    if (!running)
                        break;
                }

                if (running)
                {
                    minCost = cost;
                    Console.Write(perm + ": " + cost + "/" + minCost);
                    Console.WriteLine();
                }
            }

            Console.CursorTop = 0;
            Console.Read();
        }
    }
}