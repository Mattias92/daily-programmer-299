﻿using System;
using System.Collections.Generic;

namespace Challenge_298_Hard_From_Maze_to_graph.Scripts
{
    public class Path
    {
        public Coordinate a { get; set; }
        public Coordinate b { get; set; }
        public List<Coordinate> path { get; set; }
        public int totalCost { get; set; }

        public Path(Coordinate a, Coordinate b)
        {
            this.a = a;
            this.b = b;
        }

        public Path(Coordinate a, Coordinate b, List<Coordinate> path, int totalCost)
        {
            this.a = a;
            this.b = b;
            this.path = path;
            this.totalCost = totalCost;
        }

        public override Boolean Equals(System.Object obj)
        {
            var p = obj as Path;

            if (obj == null)
            {
                return false;
            }

            return this.a.number.Equals(p.a.number) && this.b.number.Equals(p.b.number) || this.b.number.Equals(p.a.number) && this.a.number.Equals(p.b.number);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
