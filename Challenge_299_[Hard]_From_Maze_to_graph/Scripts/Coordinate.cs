﻿using Priority_Queue;
using System;

namespace Challenge_298_Hard_From_Maze_to_graph.Scripts
{
    public class Coordinate : FastPriorityQueueNode
    {
        public int number { get; set; }
        public int x { get; set; }
        public int y { get; set; }

        public Coordinate(int number, int x, int y)
        {
            this.number = number;
            this.x = x;
            this.y = y;
        }

        public Coordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override Boolean Equals(System.Object obj)
        {
            var coord = obj as Coordinate;

            if (obj == null)
            {
                return false;
            }

            return this.x.Equals(coord.x) && this.y.Equals(coord.y);
        }

        public override int GetHashCode()
        {
            return (((x << 16) | (x >> 16)) ^ y).GetHashCode();
        }
    }
}
