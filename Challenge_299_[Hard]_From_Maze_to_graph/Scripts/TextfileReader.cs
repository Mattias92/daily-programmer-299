﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Challenge_298_Hard_From_Maze_to_graph.Scripts
{
    public class TextfileReader
    {
        //Variables for reading the textfile.
        private string line;
        private StreamReader reader;

        //Variables for keeping count of rows and columns.
        private int amountOfColumns;

        public IEnumerable<char> ReadWholeTextfile(string path)
        {
            reader = new StreamReader(path);

            while ((line = reader.ReadLine()) != null)
            {
                amountOfColumns = line.Length;

                for (int i = 0; i < amountOfColumns; i++)
                {
                    char c = line[i];
                    yield return c;
                }
            }

            reader.Close();
        }

        public Char ReadCharAtPos(string path, Coordinate coord)
        {
            reader = new StreamReader(path);
            int rowCount = 0;

            while ((line = reader.ReadLine()) != null)
            {
                for (int i = 0; i < line.Length; i++)
                {
                    char c = line[i];

                    if (coord.x == i && coord.y == rowCount)
                    {
                        reader.Close();
                        return c;
                    }
                }

                rowCount++;
            }

            reader.Close();
            return ' ';
        }

        public List<Coordinate> FindAllNumbers(string path)
        {
            List<Coordinate> numbers = new List<Coordinate>();
            reader = new StreamReader(path);
            int rowCount = 0;

            while ((line = reader.ReadLine()) != null)
            {
                for (int i = 0; i < line.Length; i++)
                {
                    char c = line[i];
                    if (Char.IsDigit(c))
                    {
                        numbers.Add(new Coordinate((int)Char.GetNumericValue(c), i, rowCount));
                    }
                }

                rowCount++;
            }

            reader.Close();
            return numbers.OrderBy(x => x.number).ToList();
        }
    }
}
