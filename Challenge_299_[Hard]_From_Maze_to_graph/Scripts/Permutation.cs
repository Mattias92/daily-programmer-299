﻿using System;
using System.Collections.Generic;

namespace Challenge_298_Hard_From_Maze_to_graph.Scripts
{
    class Permutation
    {
        private static void Swap(ref int a, ref int b)
        {
            if (a == b)
                return;
            a ^= b;
            b ^= a;
            a ^= b;
        }

        public static List<int[]> GetPerm(int[] array)
        {
            List<int[]> perms = new List<int[]>();
            GetPerm(array, 0, array.Length - 1, perms);
            return perms;
        }

        public static void GetPerm(int[] array, int recursionDepth, int maxDepth, List<int[]> perms)
        {
            if (recursionDepth == maxDepth)
            {
                int[] perm = new int[array.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    perm[i] = array[i];
                }
                perms.Add(perm);
            }
            else
            {
                for (int i = recursionDepth; i <= maxDepth; i++)
                {
                    Swap(ref array[recursionDepth], ref array[i]);
                    GetPerm(array, recursionDepth + 1, maxDepth, perms);
                    Swap(ref array[recursionDepth], ref array[i]);
                }
            }
        }
    }
}