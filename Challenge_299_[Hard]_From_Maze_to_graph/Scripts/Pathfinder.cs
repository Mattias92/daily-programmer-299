﻿using Priority_Queue;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Challenge_298_Hard_From_Maze_to_graph.Scripts
{
    public class Pathfinder
    {
        private ConsoleWriter writer;
        private TextfileReader reader;

        public Pathfinder(TextfileReader reader, ConsoleWriter writer)
        {
            this.reader = reader;
            this.writer = writer;
        }

        public Path FindPath(Coordinate start, Coordinate goal)
        {
            FastPriorityQueue<Coordinate> frontier = new FastPriorityQueue<Coordinate>(100000);

            Coordinate curr;
            Coordinate next;
            Char c;
            int newPriority;

            Dictionary<Coordinate, Coordinate> cameFrom = new Dictionary<Coordinate, Coordinate>();
            Dictionary<Coordinate, int> costSoFar = new Dictionary<Coordinate, int>();

            cameFrom[start] = null;
            costSoFar[start] = 0;

            frontier.Enqueue(start, 0);

            while (frontier.Count != 0)
            {
                curr = frontier.Dequeue();

                if (curr.x.Equals(goal.x) && curr.y.Equals(goal.y))
                {
                    return ReconstructPath(start, goal, cameFrom, costSoFar);
                }

                int newCost = costSoFar[curr] + 1;

                //Top
                next = new Coordinate(curr.x, curr.y - 1);
                c = reader.ReadCharAtPos("Maze.txt", next);
                if (c != '#' && !cameFrom.ContainsKey(next))
                {
                    newPriority = newCost + (Math.Abs(goal.x - curr.x) + Math.Abs(goal.y - (curr.y - 1)));
                    frontier.Enqueue(next, newPriority);
                    cameFrom[next] = curr;
                    costSoFar[next] = newCost;
                }

                //Right
                next = new Coordinate(curr.x + 1, curr.y);
                c = reader.ReadCharAtPos("Maze.txt", next);
                if (c != '#' && !cameFrom.ContainsKey(next))
                {
                    newPriority = newCost + (Math.Abs(goal.x - (curr.x + 1)) + Math.Abs(goal.y - curr.y));
                    frontier.Enqueue(next, newPriority);
                    cameFrom[next] = curr;
                    costSoFar[next] = newCost;
                }

                //Down
                next = new Coordinate(curr.x, curr.y + 1);
                c = reader.ReadCharAtPos("Maze.txt", next);
                if (c != '#' && !cameFrom.ContainsKey(next))
                {
                    newPriority = newCost + (Math.Abs(goal.x - curr.x) + Math.Abs(goal.y - (curr.y + 1)));
                    frontier.Enqueue(next, newPriority);
                    cameFrom[next] = curr;
                    costSoFar[next] = newCost;
                }

                //Left
                next = new Coordinate(curr.x - 1, curr.y);
                c = reader.ReadCharAtPos("Maze.txt", next);
                if (c != '#' && !cameFrom.ContainsKey(next))
                {
                    newPriority = newCost + (Math.Abs(goal.x - (curr.x - 1)) + Math.Abs(goal.y - curr.y));
                    frontier.Enqueue(next, newPriority);
                    cameFrom[next] = curr;
                    costSoFar[next] = newCost;
                }
            }

            return null;
        }

        public Path ReconstructPath(Coordinate start, Coordinate goal, Dictionary<Coordinate, Coordinate> cameFrom, Dictionary<Coordinate, int> costSoFar)
        {
            List<Coordinate> path = new List<Coordinate>();
            Coordinate curr = cameFrom.Last().Value;
            int totalCost = costSoFar.Last().Value;

            while (curr != null)
            {
                curr = cameFrom[curr];
                if (curr != null)
                {

                }
            }

            return new Path(start, goal, path, totalCost);
        }
    }
}
