﻿using System;

namespace Challenge_298_Hard_From_Maze_to_graph.Scripts
{
    public class ConsoleWriter
    {
        private TextfileReader reader;

        public ConsoleWriter(TextfileReader reader)
        {
            this.reader = reader;
        }

        public void PrintMaze(string path)
        {
            foreach (Char c in reader.ReadWholeTextfile(path))
            {

                if (Char.IsDigit(c))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                else if (c.Equals('#'))
                {
                    Console.BackgroundColor = ConsoleColor.Gray;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                }

                Console.Write(c);
                Console.ResetColor();
            }
        }

        public void SetConsoleCursor(int left, int top)
        {
            Console.CursorLeft = left;
            Console.CursorTop = top;
        }

        public void PrintPathfinding(int left, int top)
        {
            SetConsoleCursor(left, top);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write('.');
            Console.ResetColor();
        }

        public void PrintPathfinding(int left, int top, ConsoleColor color)
        {
            SetConsoleCursor(left, top);
            Console.ForegroundColor = color;
            Console.BackgroundColor = color;
            Console.Write('.');
            Console.ResetColor();
        }
    }

}
